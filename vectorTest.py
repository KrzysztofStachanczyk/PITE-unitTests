import unittest
from zadaniepython import Vector,Matrix,MyError


class VectorClassTest(unittest.TestCase):
    """
    Test vector class using black box pattern
    """
    def setUp(self):
        self.v1=Vector([1,-2,3])
        self.v2=Vector([-4,3,2])
        self.v3=Vector([-5,-6,3])
        self.v4=Vector([6,-7,-2])

    def test_multiple_operatiorns(self):
        self.assertEqual((self.v1+self.v2*5).values,[-19, 13, 13])
        self.assertEqual((7*self.v1 + self.v2 * 4+self.v4+self.v3).values, [-8, -15, 30])
        self.assertEqual((self.v1*self.v2*self.v3).values, [20, 24, -12])
        self.assertAlmostEqual((self.v1+self.v2-self.v3).magnitude(),7.5,delta=0.05)
        self.assertAlmostEqual(self.v2*self.v1+(self.v3-self.v4).magnitude(),8.1,delta=0.05)

class VectorMethodTest(unittest.TestCase):
    """
        Test vector methods using black box pattern
    """
    def setUp(self):
        pass

    def test_constructor(self):
        testArray=[
            [],
            [0, 3, 4],
            [1,4,6],
            [7.5,-1.5],
            [5,3.0],
            [5, 3, 5, 6],
            [6,2,5,2,65,2,5,7]
        ]
        testVectors = [Vector(i) for i in testArray]
        expectedSizes=[len(i) for i in testArray]

        for vector,expectedSize,expectedValues in zip(testVectors,expectedSizes,testArray):
            self.assertEqual(vector.cols,expectedSize,
                             "Expected value of vector.col: {expected} receive: {receive}".format(
                                 expected=expectedSize, receive=len(vector.values)))
            self.assertEqual(vector.rows,1,"Expected value od vector.row is 1 receive: {receive}".format(
                receive=vector.rows
            ))

            self.assertEqual(vector.values,expectedValues,
                             "Expected values of vector : {expected} receive: {receive}".format(
                                 expected=expectedValues,
                                 receive=vector.values
                             ))
    def test_to_strint(self):
        testVectorsAndExpectedResults=[
            (Vector([]),"[ ]"),
            (Vector([1]),"[ 1 ]"),
            (Vector([-1,56]),"[ -1 56 ]"),
            (Vector([1,2.5,3]),"[ 1 2.5 3 ]")
        ]

        for vector,expectedString in testVectorsAndExpectedResults:
            self.assertEqual(str(vector),expectedString,
                             "Expected string value of Vector: {expected} receive: {receive}".format(
                                 expected=expectedString,
                                 receive=str(vector)
                             ))

    def test_add_operator_for_correct_size(self):
        testPairsAndExpectedResult=[
            (Vector([2,4]),Vector([5,2]),[7,6]),
            (Vector([2, 0]), Vector([5, 3]), [7, 3]),
            (Vector([1,-10,3]), Vector([3, 6,5]), [4,-4,8]),
            (Vector([]),Vector([]),[])
        ]

        for firstVector,secondVector,expectedResult in testPairsAndExpectedResult:
            self.assertEqual((firstVector+secondVector).values,expectedResult,
                             "Expected result of addition {first} + {second} is {expected} receive: {receive}".format(
                                 first=firstVector,
                                 second=secondVector,
                                 expected=expectedResult,
                                 receive=firstVector+secondVector
                             ))
    def test_add_operator_for_different_size(self):
        testPairs=[
            (Vector([]),Vector([3])),
            (Vector([2]),Vector([])),
            (Vector([3,4]),Vector([2])),
            (Vector([5,3,2]),Vector([3,5]))
        ]

        for firstVector,secondVector in testPairs:
            with self.assertRaises(MyError):
                firstVector + secondVector

    def test_sub_operator_for_correct_size(self):
        testPairsAndExpectedResult = [
            (Vector([7, 6]), Vector([5, 2]), [2, 4]),
            (Vector([7, 3]), Vector([5, 3]), [2, 0]),
            (Vector([3, -3, 8]), Vector([3, 6, 5]),[0, -9, 3] ),
            (Vector([]), Vector([]), [])
        ]

        for firstVector, secondVector, expectedResult in testPairsAndExpectedResult:
            self.assertEqual((firstVector - secondVector).values, expectedResult,
                             "Expected result of subtraction {first} - {second} is {expected} receive: {receive}".format(
                                 first=firstVector,
                                 second=secondVector,
                                 expected=expectedResult,
                                 receive=firstVector - secondVector
                             ))

    def test_sub_operator_for_different_size(self):
        testPairs = [
            (Vector([]), Vector([3])),
            (Vector([2]), Vector([])),
            (Vector([3, 4]), Vector([2])),
            (Vector([5, 3, 2]), Vector([3, 5]))
        ]

        for firstVector, secondVector in testPairs:
            with self.assertRaises(MyError):
                firstVector - secondVector

    def test_mul_operator_for_correct_data(self):
        testPairsAndexpectedResultsForVectorToVector=[
            (Vector([0,2,3]),Vector([2,4,5]),23),
            (Vector([-3,2,5]),Vector([7,-3,-5]),-52),
            (Vector([]),Vector([]),0),
        ]
        for firstVector,secondVector,expectedResult in testPairsAndexpectedResultsForVectorToVector:
            self.assertEqual(firstVector*secondVector,expectedResult,
                                   msg="Expected value for {first} * {second} is {expected} receive {recv}".format(
                                       first=firstVector,
                                       second=secondVector,
                                       expected=expectedResult,
                                       recv=firstVector*secondVector
                                   ))

        testPairsAndexpectedResultsForVectorToScalar=[
            (Vector([6, 7, 2]), 2, [12, 14, 4]),
            (Vector([4]), 6, [24]),
            (Vector([4, 5, 3]), 2, [8, 10, 6])
        ]

        for vector,scalar,expectedResult in  testPairsAndexpectedResultsForVectorToScalar:
            self.assertEqual((vector * scalar).values, expectedResult,
                              msg="Expected value for {first} * {second} is {expected} receive {recv}".format(
                                  first=vector,
                                  second=scalar,
                                  expected=expectedResult,
                                  recv=vector * scalar
                              ))

    def test_mul_operator_for_different_vectors_size(self):
        testPairs = [
            (Vector([]), Vector([3])),
            (Vector([2]), Vector([])),
            (Vector([3, 4]), Vector([2])),
            (Vector([5, 3, 2]), Vector([3, 5]))
        ]

        for firstVector, secondVector in testPairs:
            with self.assertRaises(MyError):
                firstVector * secondVector

    def test_rmull_operator(self):
        testPairsAndexpectedResultsForVectorToScalar = [
            (Vector([6, 7, 2]), 2, [12, 14, 4]),
            (Vector([4]), 6, [24]),
            (Vector([4, 5, 3]), 2, [8, 10, 6])
        ]

        for vector, scalar, expectedResult in testPairsAndexpectedResultsForVectorToScalar:
            self.assertEqual((scalar * vector).values, expectedResult,
                             msg="Expected value for   {second} * {first} is {expected} receive {recv}".format(
                                 first=vector,
                                 second=scalar,
                                 expected=expectedResult,
                                 recv= scalar *vector
                             ))

    def test_magnitude(self):
        testVectorsAndExpectedResult=[
            (Vector([]),0),
            (Vector([3]),3),
            (Vector([3,4]),5),
            (Vector([1.5,2]),2.5)
        ]

        for vector,expectedResult in testVectorsAndExpectedResult:
            self.assertAlmostEqual(vector.magnitude(),expectedResult,
                                   msg="Expected value for sqrt is {expected} receive {result}".format(
                                       expected=expectedResult,
                                       result=vector.magnitude()
                                   ))

    def test_cross_for_correct_data(self):
        testVectorsAndExpectedResults=[
            (Vector([0,0,0]),Vector([0,0,0]), [0,0,0]),
            (Vector([1,2,3]),Vector([4,5,6]), [-3,6,-3])
        ]
        for firstVector,secondVector,expectedResult in testVectorsAndExpectedResults:
            self.assertEqual(firstVector.cross(secondVector).values,expectedResult)

    def test_cross_for_unsupported_sizes(self):
        testVectorPairs=[
            (Vector([]),Vector([0,0,0])),
            (Vector([0,0,0]),Vector([])),
            (Vector([2,3]),Vector([3,1,3]))
        ]

        for firstVector, secondVector in testVectorPairs:
            with self.assertRaises(MyError):
                firstVector.cross(secondVector)

if __name__ == '__main__':
    unittest.main()