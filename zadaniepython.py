from math import sqrt

#na samm dole kilka przykkladow :)

# /////////////////////////////////////////////////////////////////////////
class Vector(object):
    def __init__(self, tab=[]):
        self.values = []
        for num in tab:
            self.values.append(num)
        self.cols = len(tab)
        self.rows = 1

    def __str__(self):
        s = "[ "
        for num in self.values:
            s += str(num) + " "
        s += "]"
        return s

    def __getVal__(self,i):
        return self.values[i]

    def __getLen__(self):
        return self.cols

    def __add__(self,other):
        try:
            if type(other) is Vector:
                temp = []
                for j in range(self.cols):
                    temp.append(self.__getVal__(j) + other.__getVal__(j))
                return Vector(temp)
            else:
                raise MyError('type')
        except MyError as error:
            error.handle()

    def __sub__(self, other):
        temp = []
        for j in range(self.cols):
            temp.append(self.__getVal__(j) - other.__getVal__(j))

        return Vector(temp)

    def __mul__(self,other):
        if type(other) is Vector:
            temp = 0
            if self.__getLen__() == other.__getLen__():
                for j in range(self.cols):
                    temp += self.__getVal__(j) * other.__getVal__(j)
                return temp
            else:
                raise MyError("Vdim")

        elif type(other) is int or type(other) is float:
            temp = []
            for j in range(self.cols):
                temp.append(self.__getVal__(j) * other)
            return Vector(temp)

        elif type(other) is Matrix:
                if self.__getLen__() == other.__getDim__()[0]:
                    temp = []
                    for i in range(other.__getDim__()[1]):
                        line = 0
                        for j in range(self.__getLen__()):
                            line += self.__getVal__(j) * other.__getVal__(j,i)
                        temp.append(line)
                    return Vector(temp)
                else:
                    raise MyError("Mdim")

    def __rmul__(self,other):
        if type(other) is int or type(other) is float:
            temp = []
            for j in range(self.cols):
                temp.append(self.__getVal__(j) * other)
            return Vector(temp)

    def magnitude(self):
        temp = 0
        for i in range(self.cols):
            temp += self.__getVal__(i)**2
        return sqrt(temp)

    def cross(self,other):
        try:
            if type(other) is Vector:
                if self.cols == other.cols and self.cols == 3:
                    ax = self.__getVal__(1)*other.__getVal__(2) - self.__getVal__(2)*other.__getVal__(1)
                    ay = -self.__getVal__(0)*other.__getVal__(2) + self.__getVal__(2)*other.__getVal__(0)
                    az = self.__getVal__(0)*other.__getVal__(1) - self.__getVal__(1)*other.__getVal__(0)
                    return Vector([ax,ay,az])
                else:
                    raise MyError('not3D')
            else:
                raise MyError('type')
        except MyError as error:
            error.handle()



# /////////////////////////////////////////////////////////////////////////


class Matrix(object):
    def __init__(self,tab=[[0,0],[0,0]]):
        self.table = []
        try:
            for i in range(len(tab)-1):
                if len(tab[i]) != len(tab[i+1]):
                    raise MyError("Mrowslen")

            for row in tab:
                line = []
                for num in row:
                    line.append(num)
                self.table.append(line)

        except MyError as error:
            error.handle()

    def __getDim__(self):
        rows = len(self.table)
        cols = len(self.table[0])
        return [rows, cols]

    def __getVal__(self,i,j):
        return self.table[i][j]

    def __getRow__(self,i):
        return self.table[i]

    def __getCol__(self,j):
        temp = []
        for row in self.table:
            temp.append(row[j])
        return temp

    def __str__(self):
        s = ""
        for row in self.table:
            s+="|"
            for num in row:
                s += "\t" + str(num)
            s+= "\t|\n"
        return s

    def __add__(self,other):
        try:
            if type(other) is Matrix:
                if self.__getDim__()[0] == other.__getDim__()[0] and self.__getDim__()[1] == other.__getDim__()[1]:
                    temp = []
                    for i in range(self.__getDim__()[0]):
                        line = []
                        for j in range(self.__getDim__()[1]):
                            line.append(self.__getVal__(i,j) + other.__getVal__(i,j))
                        temp.append(line)
                    return Matrix(temp)
                else:
                    raise MyError("Mdim")
            else:
                raise MyError('type')
        except MyError as error:
            error.handle()

    def __sub__(self, other):
        try:
            if type(other) is Matrix:
                if self.__getDim__()[0] == other.__getDim__()[0] and self.__getDim__()[1] == other.__getDim__()[1]:
                    temp = []
                    for i in range(self.__getDim__()[0]):
                        line = []
                        for j in range(self.__getDim__()[1]):
                            line.append(self.__getVal__(i,j) - other.__getVal__(i,j))
                        temp.append(line)
                    return Matrix(temp)
                else:
                    raise MyError("Mdim")
            else:
                raise MyError('type')
        except MyError as error:
            error.handle()

    def __mul__(self,other):
        try:
            if type(other) is int or type(other) is float :
                temp = []
                for i in range(self.__getDim__()[0]):
                    line = []
                    for j in range(self.__getDim__()[1]):
                        line.append(self.__getVal__(i,j) * other)
                    temp.append(line)
                return Matrix(temp)

            elif type(other) is Matrix:
                if self.__getDim__()[1] == other.__getDim__()[0]:
                    temp = []
                    for i in range(self.__getDim__()[0]):
                        line = []
                        for j in range(self.__getDim__()[1]):
                            line.append(Vector(self.__getRow__(i))*Vector(other.__getCol__(j)))
                        temp.append(line)
                    return Matrix(temp)
                else:
                    raise MyError("Mdim")

            elif type(other) is Vector:
                if self.__getDim__()[1] == other.__getLen__():
                    temp = []
                    for i in range(self.__getDim__()[0]):
                        line = 0
                        for j in range(self.__getDim__()[1]):
                            line += self.__getVal__(i,j) * other.__getVal__(j)
                        temp.append(line)
                    return Vector(temp)
                else:
                    raise MyError("Mdim")

            else:
                raise MyError('type')
        except MyError as error:
            error.handle()

    def __rmul__(self,other):
        try:
            if type(other) is int or type(other) is float :
                temp = []
                for i in range(self.__getDim__()[0]):
                    line = []
                    for j in range(self.__getDim__()[1]):
                        line.append(self.__getVal__(i,j) * other)
                    temp.append(line)
                return Matrix(temp)

            else:
                raise MyError('type')
        except MyError as error:
            error.handle()





# /////////////////////////////////////////////////////////////////////////

class MyError(Exception):
    def __init__(self,key):
        self.s = key

    def handle(self):
        if self.s == 'type':
            print("Incorrect. Wrong types!")

        if self.s == 'Vdim':
            print("Incorrect. Wrong vector dimensions!")

        if self.s == "Mrowslen":
            print("Incorrect. All matrix rows must have equal lengths!")

        if self.s == "Mdim":

            print("Incorrect. Matrix dimensions don't match.")
        if self.s == "not3D":
            print("Incorrect. Cross product can be calculated only for 3D vectors!")


# /////////////////////////////////////////////////////////////////////////

# przyklad uzycia wektorow

#v1 = Vector([1,2,3])
#v2 = Vector([4,5,6])
#print("v1")
#print(v1)
#print("v2")
#print(v2)
#print("v1 x v2")
#v8 = v1.cross(v2)
#print(v8)

#m = Matrix([[1,2,3],[4,5,6],[7,8,1]])
#print("m")
#print(m)

#print("m*v1")
#print(m*v1)

#print("m*m")
#print(m*m)

#print("v1*m")
#print(v1*m)

#m1 = Matrix([[1,2], [3,4], [5,6]])
#print("m1")
#print(m1)


#print("v1*m1")
#print(v1*m1)


#print("m1*m1")
#print(m1*m1)


#m2 =  Matrix([[1,2], [3,4]])
#print("m2")
#print(m2)


#print ("m2*m2")
#print(m2*m2)