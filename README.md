# Pite PythonUnitTest
## Cel:
Zadaniem było przetestować dwie dostarczone przez inną osobę klasy pod kątem ich poprawności. Należało wykorzystać wbudowane w język Python narzędzie Unit testing framework.
## Wstępne założenia i narzędzia:
- środowiskiem pracy jest PyCharm
- stosuje czarno-skrzynkowy wariant testów jednostkowych, który powinien być odpowiedni dla tak prostych struktur jakimi są wektory i macierze. Ich zachowanie musi być zgodne z ogólnie przyjętymi standardami.
- po udamym wykonaniu testów metod dla reprezentantów zbioru poprawnych argumentów przechodzę do testowania kilku losowo wybranych kominacji metod - testowania klasy.
- testuje dwa rozdzaje argumentów dopuszczalne oraz zabronione - w tym  zagadnieniu ciężko doszukać się zbioru granicznego, dość często obecnego w przypadku testów jednostkowych.
- każdy test (metoda testująca) jest niezależna od innych.
- jako źródło poprawnych operacji i zachowań wykorzystuje narzędzie Octave

## Testy metod klasy Vector:
### Scenariusze testowe:
- dla poprawnych danych wejściowych (argumentów) - spodziewany wynik zgodny z zasadami operacji na wektorach

Testowany | Nazwa medody testującej | Dane testowe | Warunki zaliczenia | Wynik 
--- | --- | --- | --- | --- 
konstruktor | test_constructor | listy reprezentujące wektory | zgodność vector.col (długość), vector.values(lista wartości), vector.row(szerokość == 1) | zaliczony
konwersja na string | test_to_strint | różne wektory | poprawna zamiana wektora na String w formacie "[ x y .. z ]"| zaliczony  
operator + | test_add_operator_for_correct_size | pary wektorów pasujących długością | poprawny wynik operacji | zaliczony
operator - | test_sub_operator_for_correct_size | j.w. | j.w | zaliczony
operator * | test_mul_operator_for_correct_data | pary wektorów pasujących długością + pary wektor-skalar | wektor-wektor poprawna wartość iloczynu skalarnego, wektor-scalar odpowiednie przeskalowanie wektora | zaliczony
operator r * (mnożenie skalar*wektor) | test_rmull_operator | pary skalar wektor | odpowiednie przeskalowanie wektora | zaliczony
magnitude() | test_magnitude | różne wektory | zgodność zwróconej wartości z oczekiwaną długością wektora (z dokładnością do 0.5) | zaliczony 
cross() | test_cross_for_correct_data | pary wektorów | zgodność zwróconego wektora z iloczynem wektorowym A x B | zaliczony 

- dla danych (argumentów) błędnych (np. sumowanie wektorów o różnej długości). Jako spodziewane zachowanie przyjmuję rzucenie wyjątku MyError.

Testowany | Nazwa metody testującej | Dane testowe | Wynik
--- | --- | --- | ---
operator+ | test_add_operator_for_different_size | pary wektorów | nie rzuca wyjątku
operator- | test_sub_operator_for_different_size | pary wektorów | nie rzuca wyjątku
operator* | test_mul_operator_for_different_vectors_size | pary wektorów | zaliczony
cross() | test_cross_for_unsupported_sizes | pary wektorów | nie rzuca wyjątku

###Wnioski:
O ile operacje na dopuszczalnych danych były obsługiwane w sposób poprawny i nie budziły zastrzeżeń to próby wykonania niedozwolonych czynności posiadają błędy. 

Po wykonaniu szybkiej inspekcji kodu okazuje się, że posiadają one mechanizm rzucania oczekiwanych w tej sytuacji wyjątków, które z nie do końca jasnych przyczyn są przechwytywane i obsługiwane wewnątrz funkcji. 

Sytuacja taka nie powinna mieć miejsca ze względu na to, że na tym poziomie nie jest możliwe obsłużenie rzucanych wyjątków w sposób mający sens ponieważ wiedza o tym do czego wykorzystywany jest wynik operacji i skąd pochodzą dane jest zerowa. 

Zaleca się zatem usunięcie bloków try-except wewnątrz metod __add__ , __sub__ , cross() i ponowne wykonanie testów jednostkowych.

## Testy klasy Vector:
Celem przetestowania klasy wybrano kilka przekształceń składających się z wielu wywołań różnych operacji dostępnych w klasie Vector i porównano wynik z wartością zwróconą przez program Octave. 

### Scenariusze testowe:
Wyrażenie | Testowane metody | Wynik 
--- | --- | ---
[1,-2,3] + [-4,3,2]*5 | \_\_add\_\_ , \_\_mul\_\_ , konstruktor | zaliczony
7*[1,-2,3] + [-4,3,2] + 4*[6,-7,-2] + [-5,-6,3] | \_\_add\_\_ , \_\_rmul\_\_ , konstruktor | zaliczony 
[1,-2,3]*[-4,3,2]*[-5,-6,3] | \_\_mul\_\_ , konstruktor | zaliczony
len([1,-2,3]+[-4,3,2]-[-5,-6,3]) | \_\_add\_\_ , \_\_sub\_\_ , magnitude() , konstruktor | zaliczony
len([-4,3,2]*[1,-2,3]+([-5,-6,3]-[6,-7,-2]) | \_\_add\_\_ , \_\_sub\_\_ , magnitude(), \_\_mul\_\_, konstruktor | zaliczony

### Wnioski:
Wszystkie przetestowane kombinacje wywołań dały oczekiwnany (poprawny) wynik. Należy jednak pamiętać, że ze względu na wcześniejsze wykrycie problemów z obsługą błędów w poszczególnych metodach testowanie przebiegało tylko dla takich zbiorów wektorów w których na żadnym etapie nie wymagano wykonania operacji niepoprawnej ze względu na zasady operacji na wektorach.

## Testy metod klasy Matrix:

### Scenariusze testowe:
- dla poprawnych danych wejściowych (argumentów) - spodziewany wynik zgodny z zasadami operacji na macierzach

Testowany | Nazwa medody testującej | Dane testowe | Warunki zaliczenia | Wynik 
--- | --- | --- | --- | --- 
konstruktor | test_constructor_for_correct_data | listy dwuwymiarowe liczb | zgodność zawartości matrix.table z podanym argumentem oraz \_\_getDim() zwraca odczekiwany rozmiar macierzy |  zaliczony
operator + | test_add_operator_for_correct_data | pary macierzy o tych samych rozmiarach | poprawny wynik operacji | zaliczony
operator - | test_sub_operator_for_correct_data | j.w. | j.w. | zaliczony
operator * | test_mul_operator_for_correct_data | pary macierz-skalar, macierz-macierz o wymiarach pozwalających na wymnożenie zgodnie z zasadami mnożenia macierzy | j.w. |błąd przy mnożeniu macierz-macierz

- dla danych (argumentów) błędnych (np. sumowanie macierzy o różnych  rozmiarach). Jako spodziewane zachowanie przyjmuję rzucenie wyjątku MyError.

Testowany | Nazwa metody testującej | Dane testowe | Wynik
--- | --- | --- | ---
konstruktor | test_constructor_for_wrong_data | listy 2D liczb | nie rzuca wyjątku
operator + | test_add_operator_for_wrong_data | pary macierzy | nie rzuca wyjątku 
operator * |  test_mul_operator_for_wrong_size | pray macierzy | nie rzuca wyjątku


### Wnioski:
Podobnie jak w przypadku klasy Vector większość metod przechodzi testy dla poprawnych argumentów. Wyjątkiem jest operator * dla pary macierz-macierz, który zwraca błędny wynik i należy to poprawić. 

W przypadku błędnych argumentów (niedopuszczalnych ze względu na zasady operacji na macierzach) wyjątki są rzucane natomiast ich przechwyt następuje jeszcze w ciele funkcji co nie jest pożądanym zachowaniem. 

Podobnie jak w przypadku klasy Vector zaleca się usunięcie przechwytywania wyjątków i powtórzenie testów.


## Wnioski końcowe:
Po przeprowadzeniu testów okazuję się, że większość funkcjonalności jest już dostarczona i wymagana jest tylko drobna korekta mechanizmu obsługi błędów. 
