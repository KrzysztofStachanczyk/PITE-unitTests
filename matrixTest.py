import unittest
from zadaniepython import Vector,Matrix,MyError

class MatrixMethodTest(unittest.TestCase):
    """
    Test matrix methods using black box pattern
    """
    def test_constructor_for_correct_data(self):
        testArrays=[
            [[2,4,5],[5,6,2]],
            [[5,1,4],[3,7,3],[5,3,1]],
            [[4,2],[3,4]],
            [[]],
            [[3,4],[1,2],[4,1]]
        ]

        for array in testArrays:
            matrix=Matrix(array)
            self.assertEqual(matrix.table,array)
            self.assertEqual(matrix.__getDim__(),[len(array),len(array[0])])

    def test_constructor_for_wrong_data(self):
        testArrays=[
            [[2,3],[3]],
            [[3],[5,6]],
            [[],[3,4]]
        ]
        for array in testArrays:
            with self.assertRaises(MyError):
                Matrix(array)

    def test_add_operator_for_correct_data(self):
        testArraysAndResult=[
            ([[2,3],[1,2]],[[-7,-10],[-4,5]],[[-5,-7],[-3,7]]),
            ([[22, 3], [11, 20],[-5,-11]], [[-5, -17], [-9, 5],[-5,-8]],[[17,-14],[2,25],[-10,-19]]),
            ([[3,2,4],[-5,-3,-13]],[[4,3,12],[-4,-32,0]],[[7,5,16],[-9,-35,-13]]),
            #([],[],[])
        ]
        for first,second,expectedResult in testArraysAndResult:
            self.assertEqual((Matrix(first)+Matrix(second)).table,expectedResult)

    def test_add_operator_for_wrong_data(self):
        testArrays=[
            ([[-2,4],[3,4]],[[3,4],[4,5],[11,0]]),
            ([[3,5,2]],[[4,5]]),
            ([[4,2]],[[3,4,5]])
        ]
        for first,second in testArrays:
            with self.assertRaises(MyError):
                Matrix(first)+Matrix(second)

    def test_sub_operator_for_correct_data(self):
        testArraysAndResult=[
            ([[-5, -7], [-3, 7]],[[2, 3], [1, 2]], [[-7, -10], [-4, 5]] ),
            ([[17, -14], [2, 25], [-10, -19]],[[22, 3], [11, 20], [-5, -11]], [[-5, -17], [-9, 5], [-5, -8]]),
            ([[7, 5, 16], [-9, -35, -13]],[[3, 2, 4], [-5, -3, -13]], [[4, 3, 12], [-4, -32, 0]]),
            #([], [], [])
        ]

        for first,second,expectedResult in testArraysAndResult:
            self.assertEqual((Matrix(first)-Matrix(second)).table,expectedResult)

    def test_mul_operator_for_correct_data(self):
        testArraysAndValuesAndResults = [
            ([[3, -5, 7]], 3, [[9, -15, 21]]),
            ([[2, 3], [7, 2]], 2, [[4, 6], [14, 4]])
        ]
        for array, value, expected in testArraysAndValuesAndResults:
            result = Matrix(array) * value
            self.assertEqual(result.table,expected)

        testArraysAndResults=[
            ([[-5,4],[-4,5],[2,3]],[[0,2,4],[2,3,4]],[[8,2,-4],[10,7,4],[6,13,20]]),
            ([[-5, 4], [-4, 5]], [[0, 3, 3], [2, 4, 9]], [[8, 1, 21], [10, 8, 33]]),
            ([[1],[2],[3]],[4,5,6])
        ]

        for first,second,expected in testArraysAndResults:
            #print(Matrix(first).__str__()+"\n\n"+Matrix(second).__str__()+"\n\n"+Matrix(expected).__str__())
            result=(Matrix(first)*Matrix(second)).table
            self.assertEqual(result,expected)

    def test_mul_operator_for_wrong_size(self):
        testArrays=[
            ([[-5,4],[0,4]],[[3,4,5]]),
            ([[0],[-5],[7]],[[3],[8]]),
            ([[0,3,5],[7,3,2]],[[2,3]])
        ]

        for first,second in testArrays:
            with self.assertRaises(MyError):
                Matrix(first)*Matrix(second)



if __name__ == '__main__':
    unittest.main()